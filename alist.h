#ifndef __ALIST__H__INCLUDED__
#define __ALIST__H__INCLUDED__

#include <stdexcept>
#include "list.h"

using namespace std;

template<typename E>

class AList : public List<E>{
	private:
		int max_size; 
		int list_size;
		E* list_array;
		int curr_pos;
	public:
		AList(int size) : max_size(size), list_size(0), curr_pos(0), list_array(new E[size]){}
		~AList() {
			delete[] list_array;
		}
		void clear() {
			list_size = curr_pos = 0;
			delete[] list_array;
			list_array = new E[max_size];
		}
		void insert(const E& element) {
			if(list_size>=max_size)
				throw length_error("Exceeding maximum size limit allotted to this List!");
			list_size++;
			for(int i=list_size-1; i>curr_pos; i--)
				list_array[i] = list_array[i-1];
			list_array[curr_pos] = element;
		}	
		E delete_elem() {
			if(list_size==0)
				throw length_error("No element to delete!");
			E deleted_element = list_array[curr_pos];
			for(int i=curr_pos; i<list_size-1; i++)
				list_array[i] = list_array[i+1];
			list_size--;
			return deleted_element;
		}
		void append(const E& element) {
			if(list_size>=max_size)
				throw length_error("Exceeding maximum size limit allotted to this List!");
			list_size++;
			list_array[list_size-1] = element;
		}
		void move_to_start() {
			curr_pos = 0;
		}
		void move_to_end() {
			curr_pos = list_size - 1;
		}
		int current_pos() {
			return curr_pos;
		}
		void prev() {
			if(curr_pos==0)
				throw out_of_range("Current position already at start!");
			curr_pos--;
		}
		void next() {
			if(curr_pos==list_size-1)
				throw out_of_range("Current position already at end!");
			curr_pos++;
		}
		void move_to_pos(int pos) {
			if((pos<0) || (pos>=list_size))
				throw out_of_range("Position to move not possible!");
			curr_pos = pos;
		}
		int length() const {
			return list_size;
		}
		const E& get_value() const {
			return list_array[curr_pos];
		}
};
#endif
