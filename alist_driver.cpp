#include <iostream>
#include "alist.h"

using namespace std;

int main() {
	AList<int> list_array(5);
	cout<<list_array.length()<<endl;
	list_array.append(123);
	list_array.append(13);
	cout<<list_array.current_pos()<<endl;
	list_array.append(23);
	list_array.next();
	cout<<list_array.current_pos()<<endl;
	list_array.append(1123);
	list_array.append(1423);
	cout<<list_array.length()<<endl;
	for(list_array.move_to_start(); list_array.current_pos()!=list_array.length(); list_array.next())
		cout<<list_array.get_value()<<endl;
}
