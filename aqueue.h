#ifndef __AQUEUE__H__INCLUDED__
#define __AQUEUE__H__INCLUDED__

#include "queue.h"
#include "llist.h" // To get the link class definition

template <typename E>
class AQueue : public Queue<E> {
	private:
		int front, rear, size, length;
		E* queue_array;
	public:
		AQueue(int size) {
			this->size = size;
			queue_array = new E[size];
			front=0;
			rear=-1;
			length = 0;
		}
		~AQueue() {
			delete[] queue_array;
		}
		void clear() {
			front=rear=-1;
			length = 0;
		}
		E deque() {
			if(length==0)
				throw length_error("Queue is empty!");
			E elem = queue_array[front];
			front = (front+1)%size;
			length--;
			return elem;
		}
		void enqueue(const E& elem) {
			if(length==size)
				throw length_error("Queue is full!");
			rear = (rear+1)%size;
			queue_array[rear] = elem;
			length++;
		}
		int queue_size() const {
			return length;
		}
		const E& front_value() const {
			if(length==0)
				throw length_error("Queue is empty!");
			return queue_array[front];
		}
	};	
#endif
