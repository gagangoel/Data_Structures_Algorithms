#include <iostream>
#include "aqueue.h"

using namespace std;

int main() {
	AQueue<int> queue(6);
	queue.enqueue(10);
	queue.enqueue(11);
	queue.enqueue(12);
	queue.enqueue(13);
	queue.enqueue(14);
	queue.enqueue(15);
	cout<<queue.front_value()<<endl;
	queue.deque();
	cout<<queue.front_value()<<endl;
	queue.deque();
	cout<<queue.front_value()<<endl;
	queue.deque();
	cout<<queue.front_value()<<endl;
	queue.deque();
	cout<<queue.front_value()<<endl;
	queue.deque();
	cout<<queue.front_value()<<endl;
	queue.deque();
	queue.enqueue(16);
	cout<<queue.front_value()<<endl;
	queue.clear();
	queue.deque();
}
