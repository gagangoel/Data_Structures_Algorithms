// Implementation of the backtrack algorithm and it's applications based on Skiena's book
#include <iostream>
#include <vector>

using namespace std;

bool is_a_solution(const vector<int>& a, const int&k, const int& n) {
  return (k==(n-1));
}

// For all subsets
//void process_solution_subsets(const vector<int>& a, const int& k, const int& n) {
void process_solution(const vector<int>& a, const int& k, const int& n) {

  int counter = 0; // a counter just for formatting the output
  cout<<"{";
  for(int i=0; i < n; i++) {
    if(a[i] == true) {
      if(counter != 0)
        cout<<",";
      cout<<(i+1);
      counter++;
    }
  }
  cout<<"}"<<endl;

}

// For permutations
void process_solution_permutations(const vector<int>& a, const int& k, const int& n) {
  for(int i=0; i<n; i++)
    cout<<a[i];
  cout<<endl;
}

// For all subsets
//void construct_candidates_subsets(const vector<int>& a, const int& k, const int& n, vector<int>& c, int& n_candidates) {
void construct_candidates(const vector<int>& a, const int& k, const int& n, vector<int>& c, int& n_candidates) {
  c[0] = true;
  c[1] = false;
  n_candidates = 2;
}

// For all permutations
void construct_candidates_permutations(const vector<int>& a, const int& k, const int& n, vector<int>& c, int& n_candidates) {
  vector<bool> in_perm(n);
  for(int i=0; i <= k-1; i++) {
    in_perm[a[i]-1] = true;
  }
  n_candidates = 0;
  for(int i=0; i < n; i++) {
    if(in_perm[i] == false) {
      c[n_candidates] = i+1;
      n_candidates++;
    }
  }
}

void make_move(vector<int> a, int k, int n) {}
void unmake_move(vector<int> a, int k, int n) {}

void backtrack(vector<int> a, int k, int n) {

  if(is_a_solution(a, k, n)) {
    process_solution(a, k, n);
  }
  else {
    k++;
    vector<int> c(n);
    int n_candidates;
    construct_candidates(a, k, n, c, n_candidates);
    for(int i=0; i < n_candidates; i++) {
      a[k] = c[i];
      make_move(a, k, n);
      backtrack(a, k, n);
      unmake_move(a, k, n);
    }
  }
}

void generate_subsets(int n) {
  vector<int> a(n);
  backtrack(a, -1, n);
}

void generate_permutations(int n) {
  vector<int> a(n);
  backtrack(a, -1, n);
}

int main() {
  generate_subsets(3);
//  generate_permutations(4);
}
