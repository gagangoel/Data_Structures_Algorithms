#ifndef __BINARY__NODE__H__INCLUDED__
#define __BINARY__NODE__H__INCLUDED__

#include <stdexcept>

template<typename K, typename E>
class BinNode {
	private:
		E element;
		K key;
		BinNode *left_child, *right_child;
		bool is_leaf;
	public:
		BinNode() : left_child(nullptr), right_child(nullptr), is_leaf(true) {}
		~BinNode() {}
		void set_element(const E& elem) {
			element = elem;
		}
		const E& get_element() const {
			return element;
		}
		void set_key(const K& key) {
			this->key = key;
		}
		const K& get_key() const {
			return key;
		}
		void set_left_child(BinNode* left) {
			left_child = left;
//			is_leaf = false;
		}
		BinNode* get_left_child() const {
			return left_child;
		}
		void set_right_child(BinNode* right) {
			right_child = right;
//			is_leaf = false;
		}
		BinNode* get_right_child() const {
			return right_child;
		}
		bool is_leaf_node() {
			return (left_child==nullptr && right_child==nullptr);
		}
	};

template<typename K, typename E>
class BST : public BinNode<K, E> {
	private:
		int size;
		BinNode<K, E>* r;
//		BinNode<K, E>* find_parent(BinNode<K, E>* root) {
//			if(root == nullptr)
//				return nullptr;
			
		BinNode<K, E>* find_helper(const K& key, BinNode<K, E>* root, BinNode<K, E>* parent) {
			if(root == nullptr)
				return nullptr;
			if(root->get_key() == key) {
				if(root == r)
					return r;
				else
					return parent;
			}
			if(key<root->get_key())
				return find_helper(key, root->get_left_child(), root);
			else
				return find_helper(key, root->get_right_child(), root);
		}
		BinNode<K, E>* find_min_helper(BinNode<K, E>* root, BinNode<K, E>* parent) {
			if(root == nullptr)
				return nullptr;
			if(root->get_left_child() != nullptr)
				return find_min_helper(root->get_left_child(), root);
			else
				return parent;
		}
		BinNode<K, E>* find_max_helper(BinNode<K, E>* root) {
			if(root == nullptr)
				return nullptr;
			if(root->get_right_child()!=nullptr)
				return find_max_helper(root->get_right_child());
			else
				return root;
		}
		void clear_helper(BinNode<K, E>* root) {
			if(root==nullptr)
				return;
			if(root->get_left_child()!=nullptr)
				clear_helper(root->get_left_child());
			if(root->get_right_child()!=nullptr)
				clear_helper(root->get_right_child());
			delete root;
			r = nullptr;
			return;
		}
		void print_helper(BinNode<K, E>* root, int level) {
			if(root == nullptr)
				return;
			print_helper(root->get_left_child(), level+1);
			for(int i=0; i<level; i++)
				std::cout<<" ";
			std::cout<<root->get_key()<<std::endl;
			print_helper(root->get_right_child(), level+1);
		}
		void insert_helper(const K& key, const E& elem, BinNode<K, E>* root, BinNode<K, E>* parent) {
			if(root==nullptr) {
				root = new BinNode<K, E>();
				root->set_key(key);
				root->set_element(elem);
//				root->set_left_child(nullptr);
//				root->set_right_child(nullptr);
				size++;
				if(parent==nullptr)
					r = root;
				else {
					if(key<parent->get_key())
						parent->set_left_child(root);
					else
						parent->set_right_child(root);
				}
			}
			else if(root->get_key() == key)
				return;
			else {
				if(key < root->get_key()) {
					insert_helper(key, elem, root->get_left_child(), root);
				}
				else
					insert_helper(key, elem, root->get_right_child(), root);
			}
		}
	public:
		BST() : size(0), r(nullptr) {}
		~BST() {
			clear();
		}
		void insert(const K& key, const E& elem) {
			insert_helper(key, elem, r, nullptr);
		}
		bool find(const K& key) {
			if(size==0)
				throw std::logic_error("Tree is empty!");
			if(find_helper(key, r, nullptr)==nullptr)
				return false;
			else
				return true;
		}
		E remove(const K& key) {
			bool is_left=true;
			if(size==0)
				throw std::logic_error("Tree is empty!");
			BinNode<K,E> *parent=nullptr, *temp=nullptr;
			if(r->get_key()==key) {
				temp = r;
				parent = nullptr;
			}
			else
				parent = find_helper(key, r, nullptr);
			E elem;
			if(parent==nullptr && temp!=r)
				throw std::logic_error("Element with this key does not exist");
			else {
				if(parent!=nullptr) {
					if(parent->get_left_child()->get_key()==key)
						temp = parent->get_left_child();
					else {
						is_left=false;
						temp = parent->get_right_child();
					}
					elem = temp->get_element();
				}
			}
			if(temp->is_leaf_node()) {
				std::cout<<"deleting the leaf node."<<std::endl;
				if(parent==nullptr) {
					r = nullptr;
					delete temp;
					size--;
					return elem;
				}
				if(is_left)
					parent->set_left_child(nullptr);
				else
					parent->set_right_child(nullptr);
				delete temp;
				size--;
				return elem;
			}
			if((temp->get_left_child()!=nullptr) && temp->get_right_child()==nullptr) {
				if(parent==nullptr) {
					r = temp->get_left_child();
					delete temp;
					size--;
					return elem;
				}
				else {
					BinNode<K, E>* temp2 = temp;
					temp = temp->get_left_child();
					delete temp2;
					size--;
					if(is_left)
						parent->set_left_child(temp);
					else
						parent->set_right_child(temp);
					return elem;
				}
			}
			else if((temp->get_right_child()!=nullptr) && temp->get_left_child()==nullptr) {
				if(parent==nullptr) {
					r = temp->get_right_child();
					delete temp;
					size--;
					return elem;
				}
				else {
					BinNode<K, E>* temp2 = temp;
					temp = temp->get_right_child();
					delete temp2;
					size--;
					if(is_left)
						parent->set_left_child(temp);
					else
						parent->set_right_child(temp);
					return elem;
				}
			}
			else {
				BinNode<K, E>* temp3 = temp;
				BinNode<K, E>* min_node_parent = find_min_helper(temp->get_right_child(), temp);
				if(min_node_parent==temp) {
					BinNode<K, E>* min_node = min_node_parent->get_right_child();
					min_node->set_left_child(temp->get_left_child());
					temp = min_node;
				}
/*				if(min_node == temp->get_right_child()) {
					min_node->set_left_child(temp->get_left_child());
					temp = min_node;
				}
*/
				else {
					BinNode<K, E>* min_node = min_node_parent->get_left_child();
					BinNode<K, E>* temp4 = new BinNode<K, E>();
					temp4->set_key(min_node->get_key());
					temp4->set_element(min_node->get_element());
					temp4->set_left_child(temp->get_left_child());
					temp4->set_right_child(temp->get_right_child());
					temp = temp4;
					min_node_parent->set_left_child(nullptr);
					delete min_node;
				}
				if(parent == nullptr)
					r = temp;
				else {
					if(is_left)
						parent->set_left_child(temp);
					else
						parent->set_right_child(temp);
				}
				delete temp3;
				size--;
				return elem;
			}
		}
		int get_size() const {
			return size;
		}
		void clear() {
			if(size==0)
				return;
			clear_helper(r);
			size=0;
		}
		void print() {
			print_helper(r, 0);
		}
	};

#endif
