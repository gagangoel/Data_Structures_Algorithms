#include <iostream>
#include "bst.h"

using namespace std;

int main() {
	BST<string, int> bst;
	bst.insert("k", 11);
	bst.insert("l", 2);
	bst.insert("j", 10);
	bst.insert("a", 2);
	bst.insert("e", 2);
	bst.insert("x", 2);
	bst.insert("r", 2);
	bst.insert("o", 2);
	bst.insert("d", 2);
	bst.insert("t", 2);
	bst.print();
	cout<<bst.find("b")<<endl;
	cout<<bst.find("d")<<endl;
	cout<<bst.find("123")<<endl;
	cout<<bst.get_size()<<endl;
	bst.remove("k");
	bst.print();
	cout<<bst.get_size()<<endl;
	cout<<bst.find("k")<<endl;
	bst.clear();
	cout<<bst.get_size()<<endl;
	bst.remove("a");
//	bst.print();
//	bst.remove("c");
	bst.print();
}
