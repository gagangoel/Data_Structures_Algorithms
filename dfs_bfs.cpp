#include <iostream>
#include "graphl.h"
#include "graphm.h"
#include "aqueue.h"

using namespace std;

// IMPORTANT: a processed flag is required to accurately process all edges of the graph, both in DFS and BFS

void dfs(GraphM* graph, int vertex) {
	cout<<"Visited vertex: "<<vertex<<endl;
        // Add a funtion process_vertex_early(vertex);
	graph->set_mark(vertex, 1);
	// time = time + 1
	// entry[vertex] = time
	// if (finished) return;
	for(int w=graph->first(vertex); w<graph->n(); w=graph->next(vertex, w)) {
		/* Before we process the edge, we need to set the parent of w, since process_edge calls an edge_classification() function to classify the edge as tree or back (and also forward or cross if the graph is directed)
		if (graph->get_mark(w)==0)
			parent[w] = vertex;
		*/
		/* if (directed_graph || processed[w]==false) {
			// To avoid processing the same (undirected) edge twice, add a check here and do not process if parent[vertex] = w
			process_edge(vertex, w)
		}*/
		if(graph->get_mark(w)==0) {
			dfs(graph, w);
		}
		// if (finished) return;
	}
        // Add a funtion process_vertex_late(vertex);
        // processed[vertex] = true;
	// time = time + 1
	// exit[vertex] = time
}

/*
int edge_classification(int x, int y) {
  if (parent[y] == x) return TREE;
  if (y!=parent[x] && discovered[y]==true) return BACK;
  if (processed[y]==true && entry[x] < entry[y]) return FORWARD;
  if (processed[y]==true && entry[x] > entry[y]) return CROSS;
  cout<<"WARNING: unclassified edge: ("<<x<<","<<y<<")"<<endl;
} 
*/

// process_edge() function for DFS
// void process_edge(int x, int y) {
  // To find a cycle (i.e. to see if this edge is a back edge):
    // if (y!=parent[x] && discovered[y]==true)
        // cout << "It is a back edge, thus a cycle!";
// }

/* Topological sort using DFS for a DAG
void process_edge(int x, int y) {
  int class = edge_classification(x, y);
  if(class == BACK)
    cout<<"WARNING: Cycle detected, not a DAG!"<<endl;
}
void process_vertex_late(int vertex) {
  sorted.push(vertex);
}
void topological_sort(GraphM graph) {
  // Initialize the graph to set discovered, processed, parent arrays
  // create a stack, called sorted here
  for (int vertex = 0; vertex < graph.n(); vertex++) {
    if (discovered[vertex] == false)
      dfs(graph, vertex);
  }
  // Pop the elements from the stack and print to complete the topological sorting
}
      
*/

// A call by value to GraphM in the function below requires that GraphM have a copy constructor
void bfs(GraphM graph, int vertex) {
	AQueue<int> queue(50);
	graph.set_mark(vertex, 1);
	queue.enqueue(vertex);
	// add a parent array int* p = new int[graph.n()]
	while(queue.queue_size()!=0) {
		int v = queue.deque();
		cout<<"Visited vertex: "<<v<<endl;
                // add a function process_vertex_early(v);
		for(int w=graph.first(v); w<graph.n(); w=graph.next(v, w)) {
			/*
			if(directed_graph || processed[w]==false)
					process_edge(v, w)
			*/
			if(graph.get_mark(w)==0) {
				graph.set_mark(w, 1);
				queue.enqueue(w);
				//  set parent of w to v: p[w] = v;
			}
		}
		// add a function process_vertex_late(v);
		// processed[v] = true;
	}
}

/*
Function to find the shortest path from the root to a vertex using BFS
void find_path(int* parent, int vertex) {
  if (parent[vertex] != -1)
    find_path(parent, parent[vertex]);
  cout<<vertex<<endl;
}
*/

int main() {
	GraphM graph(10);
	graph.set_edge(0, 1, 1);
	graph.set_edge(1, 0, 1);
	graph.set_edge(0, 3, 1);
	graph.set_edge(3, 0, 1);
	graph.set_edge(0, 5, 1);
	graph.set_edge(5, 0, 1);
	graph.set_edge(0, 9, 1);
	graph.set_edge(9, 0, 1);
	graph.set_edge(1, 4, 1);
	graph.set_edge(4, 1, 1);
	graph.set_edge(1, 8, 1);
	graph.set_edge(8, 1, 1);
	graph.set_edge(3, 7, 1);
	graph.set_edge(7, 3, 1);
	graph.set_edge(2, 7, 1);
	graph.set_edge(7, 2, 1);
	cout<<"Traversing using BFS: "<<endl;
	bfs(graph, 1);
	cout<<"Traversing complete!"<<endl;
	cout<<"Traversing using DFS: "<<endl;
	dfs(&graph, 1);
	cout<<"Traversing complete!"<<endl;
}
