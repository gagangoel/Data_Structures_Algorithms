#ifndef __GRAPH__H__INCLUDED__
#define __GRAPH__H__INCLUDED__

class Graph {
	public:
		Graph() {}
		virtual ~Graph() {}
		virtual int n() = 0;
		virtual int e() = 0;
		virtual bool is_edge(int, int) = 0;
		virtual int first(int) = 0;
		virtual int next(int, int) = 0;
		virtual void init(int) = 0;
		virtual void set_edge(int, int, int) = 0;
		virtual void del_edge(int, int) = 0;
		virtual int get_weight(int, int) = 0;
		virtual int get_mark(int) = 0;
		virtual void set_mark(int, int) = 0;
};
#endif
