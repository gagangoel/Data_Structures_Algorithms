#ifndef __GRAPHL__H__INCLUDED__
#define __GRAPHL__H__INCLUDED__
#include "graph.h"
#include "llist.h"

class Edge {
	public:
		int vertex, weight;
		Edge() {
			vertex = weight = -1;
		}
		Edge(int vertex, int weight) {
			this->vertex = vertex;
			this->weight = weight;
		}
};

class GraphL : public Graph, private Edge {
	private:
		int num_vertices, num_edges;
		LList<Edge>** vertices;
		int * mark;
	public:
		GraphL(int num_vertices) {
			init(num_vertices);
		}
		~GraphL() {
			for(int i=0; i<num_vertices; i++)
				delete vertices[i];
			delete[] vertices;
		}
		void init(int num_vertices) {
			this->num_vertices = num_vertices;
			num_edges = 0;
			vertices = new LList<Edge>*[num_vertices];
			mark = new int[num_vertices];
			for(int i=0; i<num_vertices; i++) {
				vertices[i] = new LList<Edge>();
				mark[i] = 0;
			}
		}
		int n() {
			return num_vertices;
		}
		int e() {
			return num_edges;
		}
		bool is_edge(int v1, int v2) {
			if((v1>=num_vertices)||(v2>=num_vertices))
				throw out_of_range("Vertex indices out of bound!");
			for(vertices[v1]->move_to_start(); vertices[v1]->current_pos()<vertices[v1]->length(); vertices[v1]->next()) {
				if(vertices[v1]->get_value().vertex == v2)
					return true;
			}
			return false;
		}
		int first(int vertex) {
			if(vertices[vertex]->length()<=0)
				return num_vertices;
			vertices[vertex]->move_to_start();
			return vertices[vertex]->get_value().vertex;
		}
		int next(int v1, int v2) {
			if(vertices[v1]->length()<=1)
				return num_vertices;
			if(is_edge(v1, v2)) {
				if((vertices[v1]->current_pos()+1)<vertices[v1]->length()) {
					vertices[v1]->next();
					return vertices[v1]->get_value().vertex;
				}
			}
			return num_vertices;
		}
		void set_edge(int v1, int v2, int weight) {
			if(weight<=0)
				throw logic_error("Edge weight should be positive!");
			if(!is_edge(v1, v2)) {
				Edge edge(v2, weight);
				for(vertices[v1]->move_to_start(); vertices[v1]->current_pos()<vertices[v1]->length(); vertices[v1]->next()) {
					if(vertices[v1]->get_value().vertex>v2)
						break;
				}
				vertices[v1]->insert(edge);
				num_edges++;
				return;
			}
			vertices[v1]->delete_elem();
			Edge edge(v2, weight);
			vertices[v1]->insert(edge);
		}
		void del_edge(int v1, int v2) {
			if(is_edge(v1, v2)) {
				vertices[v1]->delete_elem();
				num_edges--;
			}
		}
		int get_weight(int v1, int v2) {
			if(is_edge(v1, v2))
				return vertices[v1]->get_value().weight;
			else
				return 0;
		}
		void set_mark(int v, int m) {
			if(v<num_vertices)
				mark[v] = m;
		}
		int get_mark(int v) {
			if(v<num_vertices)
				return mark[v];
			else
				return -1;
		}
};

#endif
