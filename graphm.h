#ifndef __GRAPHM__H__INCLUDED__
#define __GRAPHM__H__INCLUDED__

#include "graph.h"
#include <stdexcept>

class GraphM : public Graph {
	private:
		int** edges;
		int num_vertices;
		int num_edges;
		int * mark;
	public:
		GraphM(int num_vertices) {
			init(num_vertices);
		}
		// Copy constructor
		GraphM(GraphM& graph) {
			num_vertices = graph.num_vertices;
			num_edges = graph.num_edges;
			mark = new int[num_vertices];
			edges = new int*[num_vertices];
			for(int i=0; i<num_vertices; i++) {
				mark[i] = graph.mark[i];
				edges[i] = new int[num_vertices];
				for(int j=0; j<num_vertices; j++) {
					edges[i][j] = graph.edges[i][j];
				}
			}
		}
		~GraphM() {
			for(int i=0; i<num_vertices; i++)
				delete[] edges[i];
			delete[] edges;
		}
		void init(int num_vertices) {
			this->num_vertices = num_vertices;
			edges = new int*[num_vertices];
			mark = new int[num_vertices];
			for(int i=0; i<num_vertices; i++) {
				edges[i] = new int[num_vertices];
				mark[i] = 0;
			}
			for(int i=0; i<num_vertices; i++)
				for(int j=0; j<num_vertices; j++)
					edges[i][j] = 0;
		}
		int n() {
			return num_vertices;
		}	
		int e() {
			return num_edges;
		}
		bool is_edge(int v1, int v2) {
			if((v1<num_vertices)&&(v2<num_vertices))
				return edges[v1][v2]!=0;
			else
				throw std::out_of_range("requested vertice(s) do not exist in the graph");
		}
		int first(int v1) {
			if(v1>=num_vertices)
				throw std::out_of_range("Requested vertex does not exist in the graph!");
			for(int i=0; i<num_vertices; i++) {
				if(is_edge(v1, i))
					return i;
			}
			return num_vertices;
		}
		int next(int v1, int v2) {
			if((v1>=num_vertices)||(v2>=num_vertices))
				throw std::out_of_range("Requested vertex does not exist in the graph!");
			for(int i=v2+1; i<num_vertices; i++)
				if(is_edge(v1, i))
					return i;
			return num_vertices;
		}
		void set_edge(int v1, int v2, int weight) {
			if(weight<0)
				throw std::logic_error("Edge weight cannot be negative!");
			if((v1<num_vertices)&&(v2<num_vertices))
				edges[v1][v2] = weight;
		}
		void del_edge(int v1,  int v2) {
			if((v1<num_vertices)&&(v2<num_vertices))
				edges[v1][v2] = 0;
		}
		int get_weight(int v1, int v2) {
			if((v1<num_vertices)&&(v2<num_vertices))
				return edges[v1][v2];
			else
				return 0;
		}
		void set_mark(int v, int m) {
			if(v<num_vertices)	
				mark[v] = m;
		}
		int get_mark(int v) {
			if(v<num_vertices)
				return mark[v];
			else
				return -1;
		}
};

#endif
