#include <iostream>
#include "graphm.h"

using namespace std;

int main() {
	GraphM my_graph(12);
	cout<<"Created the graph"<<endl;
	my_graph.set_edge(2, 3, 6);
	my_graph.set_edge(2, 4, 6);
	my_graph.set_edge(2, 1, 6);
	my_graph.set_edge(3, 2, 44);
	cout<<"Number of vertices: "<<my_graph.n()<<endl;
	cout<<"Number of edges: "<<my_graph.e()<<endl;
	cout<<my_graph.is_edge(1, 2)<<endl;
	cout<<my_graph.is_edge(3, 2)<<endl;
	cout<<true<<endl;
	cout<<my_graph.get_weight(2, 3)<<endl;
	cout<<my_graph.get_weight(3, 2)<<endl;
	cout<<my_graph.get_weight(4, 1)<<endl;
	cout<<my_graph.first(2)<<endl;
	cout<<my_graph.next(2, my_graph.first(2))<<endl;
	cout<<my_graph.next(2, my_graph.next(2, my_graph.first(2)))<<endl;
	cout<<my_graph.next(2, my_graph.next(2, my_graph.next(2, my_graph.first(2))))<<endl;
	my_graph.del_edge(4, 1);
	my_graph.del_edge(3, 2);
	cout<<my_graph.get_weight(3, 2)<<endl;
}
