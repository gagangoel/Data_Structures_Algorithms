#ifndef __HEAP__H__INCLUDED__
#define __HEAP__H__INCLUDED__

#include <iostream>
#include <cmath>
#include <stdexcept>
#include <algorithm>

template<typename E, typename Comp>
class Heap {
	private:
		int size, max_size;
		E* heap;
		void sift_down(int pos, int level) {
			Comp comp;
			if(level==floor(static_cast<double>(log2(size)))+1)
				return;
			if((left_child(pos)>=0) && (right_child(pos)>=0)) {
				if(comp(heap[pos], heap[right_child(pos)])&&comp(heap[pos], heap[left_child(pos)]))
					return;
				else {
					if(comp(heap[right_child(pos)], heap[left_child(pos)])) {
						std::swap(heap[pos], heap[right_child(pos)]);
						return sift_down(right_child(pos), level+1);
					}
					else {
						std::swap(heap[pos], heap[left_child(pos)]);
						return sift_down(left_child(pos), level+1);
					}
				}
			}
			else if((left_child(pos)>=0)) {
				if(comp(heap[left_child(pos)], heap[pos])) {
					std::swap(heap[pos], heap[left_child(pos)]);
					return sift_down(left_child(pos), level+1);
				}
			}
			else if((right_child(pos)>=0)) {
				if(comp(heap[right_child(pos)], heap[pos])) {
					std::swap(heap[pos], heap[right_child(pos)]);
					return sift_down(right_child(pos), level+1);
				}
			}
			else
				return;
		}
	public:
		Heap(int max_size)  {
			this->max_size = max_size;
			heap = new E[max_size];
			size=0;
		}
		Heap(int max_size, E* input_heap, int size) {
			this->max_size = max_size;
			this->size = size;
			heap = new E[size];
			for(int i=0; i<size; i++)
				heap[i] = input_heap[i];
		}
		~Heap() {
			delete[] heap;
			max_size = size = 0;
		}
		void build_heap() {
			if(size==0)
				throw std::length_error("Heap is empty!");
			if(size==1)
				return;
			int level = floor(static_cast<double>(log2(size)));
			while(level>=1) {
				for(int i=pow(2, level-1)-1; i<=pow(2, level)-2; i++)
					sift_down(i, level);
				level--;
			}
		}
		void insert(const E& elem) {
			Comp comp;
			if(size==max_size)
				throw std::length_error("Heap is full!");
			heap[size++] = elem;
			if(size==1)
				return;
			int curr = size-1;
			while(curr!=0 && comp(heap[curr], heap[parent(curr)])) {
				std::swap(heap[parent(curr)], heap[curr]);
				curr = parent(curr);
			}
		}
		E remove_first() {
			if(size==0)
				throw std::logic_error("Heap is empty!");
			E first = heap[0];
			if(size==1) {
				size--;
				return first;
			}
			heap[0] = heap[size-1];
			size--;
			sift_down(0, 1);
			return first;
		}
		int parent(int pos) {
			if(size==0)
				throw std::logic_error("Heap is empty!");
			if(size==1)
				throw std::logic_error("Heap only has one element!");
			return ceil(static_cast<double>(pos)/2)-1;
		}
		int right_child(int pos) {
			if(size<(2*pos + 3))
				return -1;
				//throw std::logic_error("No right child for this node!");
			return 2*pos + 2;
		}
		int left_child(int pos) {
			if(size<(2*pos + 2))
				return -1;
				//throw std::logic_error("No left child for this node!");
			return 2*pos + 1;
		}
		int sibling(int pos) {
			if(pos==0)
				throw std::logic_error("This is the root node, cannot have a sibling!");
			if(pos%2 == 0)
				return pos-1;
			if(size<pos+2)
				throw std::logic_error("This node has no siblings!");
			return pos+1;
		}
		bool is_leaf(int pos) {
			if(pos>=(ceil(static_cast<double>(size)/2)+1) and pos<=size-1)
				return true;
			else
				return false;
		}
		int get_size() {
			return size;
		}
		void print() {
			if(size==0)
				return;
			for(int i=0; i<size; i++)
				std::cout<<heap[i]<<" ";
			std::cout<<std::endl;
		}
	};

#endif
