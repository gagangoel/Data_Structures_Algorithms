#include <iostream>
#include "heap.h"

using namespace std;

// Comparator class used to instantiate heap as a max-heap
template <typename E>
class MaxComparator {
	public:
		bool operator()(const E& v1, const E& v2) const {
			return v1>v2;
		}
};

int main() {
	int arr[] = {1, 2, 3, 4, 5};
//	Heap<int> heap(100, arr, sizeof(arr)/sizeof(arr[0]));
	Heap<int, MaxComparator<int>> heap(100);
	heap.print();
	cout<<heap.get_size()<<endl;
	heap.insert(5);
	heap.print();
	heap.insert(1);
	heap.print();
	heap.insert(3);
	heap.print();
	heap.insert(7);
	heap.print();
	heap.insert(2);
	heap.print();
	heap.insert(10);
	heap.print();
	heap.build_heap();
	heap.print();
	heap.remove_first();
	heap.print();
	cout<<heap.get_size()<<endl;
}
