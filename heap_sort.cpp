#include <iostream>
#include "heap.h"

using namespace std;

// Comparator class used to instantiate heap as a max-heap used for heap sort
template <typename E>
class MaxComparator {
	public:
		bool operator()(const E& v1, const E& v2) const {
			return v1>v2;
		}
};

int main() {
	int a[] = {1, 34, 1 ,34, 23, 34, 1 ,34 ,23 ,1 ,3, 54, 656, 645, 23, 1, 3, 54, 656, 54, 656, 645, 23, 1, 3, 54, 656, 645, 343, 1, 34, 3, 23, 54, 656, 645, 343};
	int length = sizeof(a)/sizeof(a[0]);
	for(int i=0; i<length; i++)
		cout<<a[i]<<" ";
	cout<<endl;
	Heap<int, MaxComparator<int>> heap(100, a, length);
	heap.build_heap();
	for(int i=length-1; i>=0; i--) {
		a[i] = heap.remove_first();
	}
	for(int i=0; i<length; i++)
		cout<<a[i]<<" ";
	cout<<endl;
}
