#include "heap.h"
#include "graphl.h"
#include <iostream>

using namespace std;

// A structure for the graph edge
class MSTEdge {
	public:
		int from, to, weight;
		MSTEdge() {
			from=to=weight=-2;
		}
		MSTEdge(int from, int to, int weight) {
			this->from = from;
			this->to = to;
			this->weight = weight;
		}
};

// A min heap comparator to be used for the edges of the graph
class MinComparator {
	public:
		bool operator()(const MSTEdge& v1, const MSTEdge& v2) {
			return v1.weight<v2.weight;
		}
};

class ParPtr {
	private:
		int* vertices;
		int num_vertices;
		const int ROOT = -1;
	public:
		ParPtr(int num_vertices) {
			this->num_vertices = num_vertices;
			vertices = new int[num_vertices];
			for(int i=0; i<num_vertices; i++)
				vertices[i] = ROOT;
		}
		~ParPtr() {
			delete[] vertices;
		}
		int root(int v1) {
			while(vertices[v1]!=ROOT) {
				v1 = vertices[v1];
			}
			return v1;
		}
		bool differ(int v1, int v2) {
			return root(v1)!=root(v2);
		}
		void unite(int v1, int v2) {
			vertices[root(v2)] = root(v1);
		}
};

MSTEdge* mst(GraphL* graph) {
	// Add the edges of the graph to the Heap
	MSTEdge * heap_edges = new MSTEdge[graph->e()];
	int edge_count=0;
	for(int i=0; i<graph->n(); i++) {
		for(int w=graph->first(i); w<graph->n(); w=graph->next(i, w)) {
			heap_edges[edge_count].from = i;
			heap_edges[edge_count].to = w;
			heap_edges[edge_count].weight = graph->get_weight(i, w);
			edge_count++;
		}
	}
	// Heapify this edge array
	Heap<MSTEdge, MinComparator> heap(graph->e(), heap_edges, graph->e());
	heap.build_heap();
	// Create an array to store the mst edges (number of edges in an MST is |V|-1)
	MSTEdge* mst_edges = new MSTEdge[graph->n()-1];
	int num_mst=graph->n();
	// Create a ParPtr object to determine if two vertices belong to the same equivalence class.
	ParPtr parptr(graph->n());
	int num_edges=0;
	while(num_mst>1) {
		MSTEdge temp = heap.remove_first();
		int from = temp.from, to = temp.to, weight = temp.weight;
		if(parptr.differ(from, to)) {
			parptr.unite(from, to);
			mst_edges[num_edges] = temp;
			num_mst--;
			num_edges++;
		}
	}
	delete[] heap_edges;
	return mst_edges;
}

int main() {
	GraphL graph(6);
	graph.set_edge(0, 2, 7);
	graph.set_edge(2, 0, 7);
	graph.set_edge(0, 4, 9);
	graph.set_edge(4, 0, 9);
	graph.set_edge(4, 5, 1);
	graph.set_edge(5, 4, 1);
	graph.set_edge(3, 2, 1);
	graph.set_edge(2, 3, 1);
	graph.set_edge(5, 3, 2);
	graph.set_edge(3, 5, 2);
	graph.set_edge(2, 5, 2);
	graph.set_edge(5, 2, 2);
	graph.set_edge(2, 1, 5);
	graph.set_edge(1, 2, 5);
	graph.set_edge(1, 5, 6);
	graph.set_edge(5, 1, 6);
	MSTEdge* mst_edges = mst(&graph);
	for(int i=0; i<graph.n()-1; i++)
		cout<<mst_edges[i].from<<", "<<mst_edges[i].to<<endl;
	delete [] mst_edges;
}
