#ifndef __LIST__H__INCLUDED__
#define __LIST__H__INCLUDED__

template<typename E>

class List {
	public:
		List() {}
		virtual ~List() {}; 
		virtual void clear() = 0;
		virtual void insert(const E&) = 0;
		virtual E delete_elem() = 0;
		virtual void append(const E&) = 0;
		virtual void move_to_start() = 0;
		virtual void move_to_end() = 0;
		virtual int current_pos() = 0;
		virtual void prev() = 0;
		virtual void next() = 0;
		virtual void move_to_pos(int pos) = 0;
		virtual int length() const = 0;
		virtual const E& get_value() const = 0;
};
#endif
