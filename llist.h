#ifndef __LLIST__H__INCLUDED__
#define __LLIST__H__INCLUDED__

#include <stdexcept>
#include "list.h"

using namespace std;

template<typename E>
class Link {
	private:
		E element;
		Link* next;
	public:
		Link(const E& elem, Link* next = nullptr) : element(elem) {
			 this->next=next;
		}
		Link(Link* next = nullptr){}
		const E& get_element() const {
			return element;
		}
		Link* get_next() const {
			return next;
		}
		void set_element(const E& elem) {
			element = elem;
		}
		void set_next(Link* next) {
			this->next = next;
		}
	};

template<typename E>
class LList : public List<E>, public Link<E>{
	private:
		int size;
		Link<E>* curr;
		Link<E>* head;
		Link<E>* tail;
	public:
		LList() {
			curr = new Link<E>();
			head=tail=curr;
			size = 0;
		}
		void clear() {
			for(Link<E>* ptr = head; ptr!=tail->get_next();) {
				curr = ptr;
				ptr = ptr->get_next();
				delete curr;
			}
			curr=new Link<E>();
			head=tail=curr;
			size=0;
		}
		void insert(const E& elem) {
			Link<E>* node = new Link<E>(elem, curr->get_next());
			curr->set_next(node);
			if(tail==curr)
				tail = curr->get_next();
			size++;
		}
		E delete_elem() {
			Link<E>* temp = curr->get_next();
			curr->set_next(temp->get_next());
			E deleted_elem = temp->get_element();
			delete temp;
			size--;
			return deleted_elem;
		}
		void append(const E& elem) {
			Link<E>* node = new Link<E>(elem);
			tail->set_next(node);
			tail = node;
			size++;
		}
		void move_to_start() {
			curr = head;
		}
		void move_to_end() {
			curr = tail;
		}
		int current_pos() {
			Link<E>* temp = head;
			int i=0;
			for(; temp!=curr;temp=temp->get_next()) 
				i++;
			return i;
		}
		void prev() {
			if(curr==head)
				throw out_of_range("Already at first position!");
			Link<E>* temp2;
			for(Link<E>* temp = head; temp!=curr; temp=temp->get_next())
				temp2=temp;
			curr = temp2;
		}
		void next() {
			curr = curr->get_next();
		}
		void move_to_pos(int pos) {
			if(pos>size-1)
				throw out_of_range("Exceeding maximum size of the list");
			int i=0;
			for(curr=head; i!=pos; i++)
				curr = curr->get_next();
		}
		int length() const {
			return size;
		}
		const E& get_value() const {
			if(curr==tail)
				throw out_of_range("No Elements in the list!");
			return curr->get_next()->get_element();
		}
	};
#endif
