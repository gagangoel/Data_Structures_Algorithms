#include <iostream>
#include "llist.h"

using namespace std;

int main() {
	LList<int> list_linked;
	list_linked.append(123);
	list_linked.append(11123);
	cout<<list_linked.current_pos()<<endl;
	cout<<list_linked.length()<<endl;
//	list_linked.move_to_end();
	cout<<list_linked.current_pos()<<endl;
	cout<<list_linked.get_value()<<endl;
	list_linked.clear();
//	cout<<list_linked.get_value()<<endl;
	cout<<list_linked.length()<<endl;
	list_linked.insert(12);
	cout<<list_linked.length()<<endl;
	cout<<list_linked.current_pos()<<endl;
	list_linked.insert(1212);
	list_linked.append(121212);
	cout<<list_linked.length()<<endl;
	cout<<list_linked.current_pos()<<endl;
	cout<<list_linked.get_value()<<endl;
	cout<<list_linked.delete_elem()<<endl;
	list_linked.clear();
	cout<<"Cleared the linked list."<<endl;
	list_linked.append(12);
	list_linked.append(21);
	cout<<list_linked.current_pos()<<endl;
	list_linked.append(221);
	list_linked.next();
	cout<<list_linked.current_pos()<<endl;
	list_linked.insert(11);
	cout<<list_linked.current_pos()<<endl;
	for(list_linked.move_to_start(); list_linked.current_pos()!=list_linked.length(); list_linked.next())
		cout<<list_linked.get_value()<<endl;
	list_linked.move_to_pos(2);
	cout<<list_linked.get_value()<<endl;
	list_linked.prev();
	cout<<list_linked.get_value()<<endl;
}
