#ifndef __LSTACK__H__INCLUDED__
#define __LSTACK__H__INCLUDED__

#include "stack.h"
#include "llist.h" // To get the link class definition

template <typename E>
class LStack : public Stack<E>, public Link<E>{
	private:
		Link<E>* top;
		int size;
	public:
		LStack() : top(nullptr), size(0) {}
		~LStack() {
			while(top!=nullptr) {
				Link<E>* tmp = top;
				top = top->get_next();
				delete tmp;
			}
		}
		void clear() {
			size = 0;
			this->~LStack();
		}
		void push(const E& elem) {
			Link<E>* node = new Link<E>(elem);
			node->set_next(top);
			top = node;
			size++;
		}
		E pop() {
			if(size==0)
				throw out_of_range("Stack is empty!");
			Link<E>* node = top;
			E elem = node->get_element();
			top = top->get_next();
			delete node;
			size--;
			return elem;
		}
		int length() const {
			return size;
		}
		const E& top_value() const {
			if(top==nullptr)
				throw out_of_range("Stack is empty!");
			return top->get_element();
		}
	};
#endif
