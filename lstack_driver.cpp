#include <iostream>
#include "lstack.h"

using namespace std;

int main() {
	LStack<string> linked_stack;
	cout<<linked_stack.length()<<endl;
	linked_stack.push("123");
	linked_stack.push("23");
	cout<<linked_stack.length()<<endl;
	cout<<linked_stack.top_value()<<endl;
	cout<<linked_stack.pop()<<endl;
	cout<<linked_stack.length()<<endl;
}
