#include <iostream>
#include <cmath>
#include <stdexcept>
using namespace std;

void merge(int* a1, int* a2, int length, int *a) {
	int i=0,j=0,k=0;
	while(i<static_cast<int>(floor(length/2)) || j<(length-static_cast<int>(floor(length/2)))) {
		if(i<static_cast<int>(floor(length/2)) && j<(length-static_cast<int>(floor(length/2)))) {
			if(a1[i]<a2[j])
				a[k++] = a1[i++];
			else
				a[k++] = a2[j++];
		}
		else if(i<static_cast<int>(floor(length/2)))
			a[k++] = a1[i++];
		else
			a[k++] = a2[j++];
	}
}
		
void merge_sort(int *a, int lower, int upper, int*b) {
	int mid = static_cast<int>(floor((upper+lower)/2));
	if(lower==upper)
		throw length_error("Array length is zero!");
	if(lower==upper-1) {
		b[0]=a[lower];
		return;
	}
	int b1[mid-lower], b2[upper-mid];
	merge_sort(a, lower, mid, b1);
	merge_sort(a, mid, upper, b2);
	return merge(b1, b2, upper-lower, b);
}

int main() {
	int a[] = {1, 34, 23, 3, 54, 656, 645, 343,1, 34, 3, 23, 54, 656, 645, 343};
	int length = sizeof(a)/sizeof(a[0]);
	int b[length];
	merge_sort(a, 0, length, b);
	for(int i=0; i<length; i++)
		cout<<b[i]<<" ";
	cout<<endl;


}
