#include <iostream>

using namespace std;

int main() {
	int a[] = {2, 3,4 , 454, 46, 12 ,34, 54, 62, 234, 54,56, 4};
	for(auto j: a)
		cout<<j<<" ";
	cout<<endl;

// Insertion Sort implementation
	int length = sizeof(a)/sizeof(a[0]);
	for(int i=1; i<length; i++) {
		for(int j=i; j>0; j--) {
			if(a[j]<a[j-1])
				swap(a[j], a[j-1]);
			else
				break;
		}
	}
	for(auto j: a)
		cout<<j<<" ";
	cout<<endl;

// Bubble sort implementation
	int b[] = {2, 3,4 , 454, 46, 12 ,34, 54, 62, 234, 54,56, 4};
	length = sizeof(b)/sizeof(b[0]);
	for(auto j: b)
		cout<<j<<" ";
	cout<<endl;
	for(int i=0; i<length-1; i++) {
		for(int j=length-1; j>i; j--) {
			if(b[j]<b[j-1])
				swap(b[j], b[j-1]);
		}
	}
	for(auto j: b)
		cout<<j<<" ";
	cout<<endl;

// Selection sort implementation
	int c[] = {3,2, 4 , 454, 46, 12 ,34, 54, 62, 234, 54,56, 4};
	length = sizeof(c)/sizeof(c[0]);
	for(auto j: c)
		cout<<j<<" ";
	cout<<endl;
	for(int i=0; i<length-1; i++) {
		int max = c[0];
		int max_index = 0;
		for(int j=0; j<length-i; j++) {
			if(max<c[j]) {
				max_index = j;
				max = c[j];
			}
		}
		if(max_index!=length-i-1)
			swap(c[max_index], c[length-i-1]);
	}
	for(auto j: c)
		cout<<j<<" ";
	cout<<endl;
}
