#ifndef __QUEUE__H__INCLUDED__
#define __QUEUE__H__INCLUDED__

template<typename E>
class Queue {
	public:
		Queue() {}
		virtual ~Queue() {}
		virtual void clear() = 0;
		virtual E deque() = 0;
		virtual void enqueue(const E&) = 0;
		virtual int queue_size() const = 0;
		virtual const E& front_value() const = 0;
	};
#endif
