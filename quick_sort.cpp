#include <iostream>
#include <algorithm>

using namespace std;

int partition(int * a, int lower, int upper, int pivot) {
	do {
		while(a[++lower]<a[pivot]);
		while((lower<upper)&&(a[pivot]<a[--upper]));
		swap(a[lower], a[upper]);
	} while(lower<upper);
	return lower;
}

void quick_sort(int *a, int lower, int upper) {
	if(lower>=upper)
		return;
	int pivot = (lower+upper)/2;
	swap(a[pivot], a[upper]);
	int new_pivot = partition(a, lower-1, upper, upper);
	swap(a[new_pivot], a[upper]);
	quick_sort(a, lower, new_pivot-1);
	quick_sort(a, new_pivot+1, upper);
}

int main() {
	int a[] = {1, 34, 1 ,34, 23, 34, 1 ,34 ,23 ,1 ,3, 54, 656, 645, 23, 1, 3, 54, 656, 54, 656, 645, 23, 1, 3, 54, 656, 645, 343, 1, 34, 3, 23, 54, 656, 645, 343};
	int length = sizeof(a)/sizeof(a[0]);
	for(int i=0; i<length; i++)
		cout<<a[i]<<" ";
	cout<<endl;
	quick_sort(a, 0, length-1);
	for(int i=0; i<length; i++)
		cout<<a[i]<<" ";
	cout<<endl;
}	
