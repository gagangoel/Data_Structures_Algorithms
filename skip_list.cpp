#include <iostream>
#include <random>

using namespace std;

template <typename E>
class SkipNode {
	public:
		E elem;
		int num_levels;
		SkipNode** forward;
		// A function that returns a random number from a geometric distribution
		int random_level() {
			random_device rd;
			default_random_engine re(rd());
			geometric_distribution<> dist;
			return dist(re);
		}
	//	SkipNode() {}
		SkipNode(const E& elem, int num_levels) {
			this->num_levels = num_levels;
			this->elem = elem;
			cout<<"Creating the forward lst: "<<this->elem<<endl;
			forward = new SkipNode*[num_levels];
			for(int i=0; i<=num_levels; i++)
				forward[i] = nullptr;
			cout<<"Still in constructor: "<<this->elem<<endl;
		}
		SkipNode(const E& elem) {
			num_levels = random_level();
			SkipNode(elem, num_levels);
		}
		~SkipNode() {
			cout<<"In the destructor: "<<elem<<endl;
			delete[] forward;
		}
/*		int get_num_levels() {
			return num_levels;
		}
		E get_element() {
			return elem;
		}
*/
};

template <typename E>
class SkipList {
	private:
		SkipNode<E>* header;
		int num_nodes;
		int level; // maximum level of any node stored
		SkipNode<E>** update;
		void update_update(const E& elem) {
			update = new SkipNode<E>*[level];
			SkipNode<E>* curr;
			SkipNode<E>* prev;
			for(int i=0; i<=level; i++) {
				curr = header->forward[i];
				cout<<curr->elem<<endl;
				prev = curr;
				while((curr!=nullptr) && (curr->elem<=elem)) {
					prev = curr;
					curr = curr->forward[i];
				}
				update[i] = prev;
			}
		}
	public:
		SkipList() {
			header = nullptr;
			num_nodes = 0;
			level = -1;
		}
		~SkipList() {
			if(num_nodes>1) {
				for(int i=0; i<=level; i++)
					delete update[i];
				delete[] update;
				delete header;
			}
		}
		bool find(const E& elem) {
			update_update(elem);
			for(int i=level; i>=0; i--) {
				if(update[i]->elem==elem)
					return true;
			}
			return false;
		}
		void insert(const E& elem) {
			SkipNode<E>* temp = new SkipNode<E>(elem);
			cout<<temp->elem<<endl;
			cout<<temp->num_levels<<endl;
			if(num_nodes == 0) {
				level = temp->num_levels;
				header = new SkipNode<E>(-1, level);
				for(int i=0; i<=level; i++)
					header->forward[i]=temp;
			}
			else {
				// Newly created node has more levels than the maximum number of levels this SkipList currently accomodates
				if(level<temp->num_levels) {
					// Reassign header to add these additional number of levels
					level = temp->num_levels;
					SkipNode<E>* new_header = new SkipNode<E>(-1, level);
					int i=0;
					for(i=0; i<=level; i++) {
						if(i<=header->num_levels)
							new_header->forward[i] = header->forward[i];
						else
							new_header->forward[i] = temp;
					}
					delete header;
					header = new_header;
				}
				else {
					for(int i=0; i<=level; i++)
						delete update[i];
					delete[] update;
					update_update(elem);
					for(int i=0; i<=temp->num_levels; i++) {
						temp->forward[i] = update[i]->forward[i];
						update[i]->forward[i] = temp;
					}
				}
			}
			cout<<temp->elem;
			num_nodes++;
		}
		void remove(const E& elem) {}
};

int main() {
	SkipList<int> sl;
	sl.insert(50);
//	cout<<sl.find(5)<<endl;
}
