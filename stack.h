#ifndef __STACK__H__INCLUDED__
#define __STACK__H__INCLUDED__

template <typename E>
class Stack {
	public:
		Stack() {}
		virtual ~Stack() {}
		virtual void push(const E&) = 0;
		virtual E pop() = 0;
		virtual void clear() = 0;
		virtual int length() const = 0;
		virtual const E& top_value() const = 0;
	};
#endif
