#include <iostream>
#include <stdexcept>
#include "graphl.h"
#include "aqueue.h"

using namespace std;

void topological_sort(GraphL* graph) {
	int count[graph->n()];
	for(int i=0; i<graph->n(); i++)
		count[i]=0;
	int i=0;
	AQueue<int> queue(graph->n());
	// Counting all incoming edges to all vertices
	for(i=0; i<graph->n(); i++) {
		for(int j=graph->first(i); j<graph->n(); j=graph->next(i, j)) {
			count[j]++;
		}
	}
	for(i=0; i<graph->n(); i++)
		if((count[i]==0)&&(graph->get_mark(i)==0)) {
			graph->set_mark(i, 1);
			queue.enqueue(i);
		}
	if(queue.queue_size()==0)
		throw logic_error("Graph has cycles!");
	while(queue.queue_size()!=0) {
		int v = queue.deque();
		cout<<"Processed vertex: "<<v<<endl;
		for(int w=graph->first(v); w<graph->n(); w=graph->next(v, w))
			count[w]--;
		for(i=0; i<graph->n(); i++)
			if((count[i]==0)&&(graph->get_mark(i)==0)) {
				graph->set_mark(i, 1);
				queue.enqueue(i);
			}
	}
	for(int i=0; i<graph->n(); i++)
		if(graph->get_mark(i)==0)
			throw logic_error("Graph has cycles!");
}

int main() {
	GraphL graph(7);
	graph.set_edge(0, 1, 1);
	graph.set_edge(0, 2, 1);
	graph.set_edge(1, 3, 1);
	graph.set_edge(1, 4, 1);
	graph.set_edge(2, 4, 1);
	graph.set_edge(4, 5, 1);
	graph.set_edge(1, 5, 1);
	graph.set_edge(5, 6, 1);
	topological_sort(&graph);
}
